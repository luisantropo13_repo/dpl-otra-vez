// Creo un método que llama a index.pug y nos muestra la array de bicicletas
let Bicicleta = require("../models/Bicicleta");

exports.bicicleta_list = function(req,res){
     res.render("index.pug", {bicis: Bicicleta.allBicis});
}