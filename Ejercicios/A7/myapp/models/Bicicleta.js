// Creamos un constructor que contenga las características de las biciletas
let Bicicleta = function (id, color, modelo, ubicacion) {

    this.id = id;

    this.color = color;

    this.modelo = modelo;

    this.ubicacion = ubicacion;

}

// Se crea una array vacia donde ir guardando las biciletas
Bicicleta.allBicis = [];

// Creamos un método para añadir las bicicletas a la array
Bicicleta.add = function (bici) {

    this.allBicis.push(bici);

}

// Añadimos un par de bicicletas con las coordenadas del ejercicio anterior. Esto lo hacemos con Hard Coding, que es una mala práctica donde se meten datos directamente en el código fuente
let a = new Bicicleta(1, "Rojo", "Trek", [28.4965, -13.8622]);

let b = new Bicicleta(2, "Azul", "Orbea", [28.4965, -13.8622]);

Bicicleta.add(a);
Bicicleta.add(b);

// Exportamos el módulo para que esté accesible
module.exports = Bicicleta;

