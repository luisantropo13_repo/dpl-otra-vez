
let Bicicleta = require("../../models/Bicicleta.js");


// exports.bicicleta_list = function (req, res) {

//     res.status(200).json({

//         bicicletas: Bicicleta.allBicis

//     });

// };

// exports.bicicleta_create = function (req, res) {

//     let bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);

//     bici.ubicacion = [req.body.latitud, req.body.longitud];

//     Bicicleta.add(bici);

//     res.status(201).json({

//         bicicleta: bici

//     });
// }

// exports.bicicleta_delete = function (req, res) {

//     Bicicleta.removeById(req.body.id);

//     res.status(204).send();

// };

// Ejercicio 4 A16
exports.bicicleta_list = function (req,res){
    Bicicleta.allBicis(function (err, bicis) {
        if (err) res.status(500).send(err.message);

        res.status(200).json({
            bicicletas: bicis
        })
    })
}

exports.bicicleta_create = function (req,res) {
    let bici = new Bicicleta({
        bicicletaID: req.body.bicicletaID,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: req.body.ubicacion
    });

    Bicicleta.add(bici,function (err,newBici){
        if (err) res.status(500).send(err.mensage);
        res.status(201).send(newBici);
    });
};


// Ejercicio 5 Tarea A16
exports.bicicleta_findById = function(req,res){
    Bicicleta.findById(req.params.id, function(err,aBici){
        if (err) res.status(500).send(err.mensage);
        res.status(201).send(aBici);
    });
};

exports.bicicleta_removeById = function(req,res){
    Bicicleta.removeById(req.params.id, function(err,aBici){
        if(err) res.status(500).send(err.mensage);
        res.status(201).send(aBici);
    });
};